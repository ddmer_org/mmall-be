package com.leno.mmall.controller.backend;

import com.github.pagehelper.PageInfo;
import com.leno.mmall.common.Const;
import com.leno.mmall.common.ResponseCode;
import com.leno.mmall.common.ServerResponse;
import com.leno.mmall.pojo.User;
import com.leno.mmall.service.IFileService;
import com.leno.mmall.service.IOrderService;
import com.leno.mmall.service.IUserService;
import com.leno.mmall.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/manage/order/")
public class OrderManageController {
    //alt+enter
    // @Resource
    @Autowired
    public IOrderService iOrderService;
    @Autowired
    public IUserService iUserService;
    @Autowired
    public IFileService iFileService;

    @RequestMapping("list.do")
    public ServerResponse<PageInfo> manageList(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                         HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iOrderService.manageList(pageNum, pageSize);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }

    //        产品搜索和动态排序
    //where 过滤 / order by 排序/ limit 分页
    @RequestMapping("search.do")
    public ServerResponse<PageInfo> manageSearch(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                           Long orderNo, HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iOrderService.manageSearch(orderNo,pageNum,pageSize);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }


    }



    @RequestMapping("detail.do")
    public ServerResponse<OrderVo> manageDetail(long orderNo,
                                          HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iOrderService.manageDetail(orderNo);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }
    @RequestMapping("send_goods.do")
    public ServerResponse<String> manageSendGood(long orderNo,
                                                 HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iOrderService.manageSendGoods(orderNo);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }




}
