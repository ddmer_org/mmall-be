package com.leno.mmall.controller.backend;

import com.leno.mmall.common.Const;
import com.leno.mmall.common.ResponseCode;
import com.leno.mmall.common.ServerResponse;
import com.leno.mmall.pojo.Category;
import com.leno.mmall.pojo.User;
import com.leno.mmall.service.ICategoryService;
import com.leno.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/manage/category/")
public class CategoryManageController {
    //alt+enter
    // @Resource
    @Autowired
    public ICategoryService iCategoryService;
    @Autowired
    public IUserService iUserService;

    //        产品搜索和动态排序
    //where 过滤 / order by 排序/ limit 分页
    @RequestMapping("get_category.do")
    public ServerResponse<List<Category>> getCategory(Integer categoryId, HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iCategoryService.getChildrenParallelCategory(categoryId);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }
//    /**
//     * 增加目录节点
//     *
//     * @param session
//     * @param parentId
//     * @param categoryName
//     * @return
//     */
    @RequestMapping("add_category.do")
    public ServerResponse addCategory(@RequestParam(value = "categoryId", defaultValue = "0")Integer categoryId,
                                      String categoryName, HttpSession session) {
        System.out.println("接收参数:" + categoryId + "," + categoryName);
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iCategoryService.addCategory(categoryName,categoryId);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }
    /**
     * 修改品类名字
     *
     * @param session
     * @param categoryId
     * @param categoryName
     * @return
     */
    @RequestMapping("set_category_name.do")
    public ServerResponse setCategoryName(Integer categoryId, String categoryName, HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iCategoryService.updateCategoryName(categoryId,categoryName);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }

    @RequestMapping("get_deep_category.do")
    public ServerResponse getDeepCategory(Integer categoryId,
                                          HttpSession session) {
        User user = (User) session.getAttribute(Const.CURRENT_ADMIN);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (iUserService.checkAdminRole(user).isSuccess()) {
            return iCategoryService.getChildrenParallelCategory(categoryId);

        } else {
            return ServerResponse.createByErrorMessage("无权限");
        }

    }





}
