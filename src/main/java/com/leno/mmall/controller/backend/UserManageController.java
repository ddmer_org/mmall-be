package com.leno.mmall.controller.backend;

import com.leno.mmall.common.Const;
import com.leno.mmall.common.ResponseCode;
import com.leno.mmall.common.ServerResponse;
import com.leno.mmall.pojo.User;
import com.leno.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

//@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value="/manage/user/",method = {RequestMethod.GET,RequestMethod.POST})
public class UserManageController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session){
        ServerResponse<User> response = userService.login(username,password);
        User user=response.getData();
        if (response.isSuccess()) {
            if (user.getRole() == Const.Role.ROLE_ADMIN) {
            // 说明登录的是管理员1 普通用户0
                session.setAttribute(Const.CURRENT_ADMIN, user);
                return response;
            } else {
                return ServerResponse.createByErrorCodeMessage(ResponseCode.ERROR.getCode(), "请以管理员身份登录");
            }
        }
        return response;
    }
}
