package com.leno.mmall;

import com.github.pagehelper.PageHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.Properties;

/*
 这里是由于 SpringBoot 加载 GsonAutoConfiguration @Configuration 类时，试图调用 GsonBuilder 的 setLenient（）方法。
但是 pom.xml 文件中直接或间接依赖的 gson jar 包的版本，并没有setLenient()方法,所以导致报错！
 */
@SpringBootApplication(exclude = {GsonAutoConfiguration.class})
//@SpringBootApplication
public class MmallApplication {

    //配置mybatis的分页插件pageHelper
     @Bean
     public PageHelper pageHelper(){
               PageHelper pageHelper = new PageHelper();
               Properties properties = new Properties();
//                properties.setProperty("offsetAsPageNum","true");
//                properties.setProperty("rowBoundsWithCount","true");
//                properties.setProperty("reasonable","true");
                properties.setProperty("dialect","mysql");    //配置mysql数据库的方言
                 pageHelper.setProperties(properties);
                return pageHelper;
            }


    public static void main(String[] args) {
        SpringApplication.run(MmallApplication.class, args);
    }

}
